var questions = [
    {'question': 'Ile hubert ma pytań?',
    'answers': ['6', '16', '45', '35'],
    'correctAnswer': '16'
    },
    {'question': 'Jak ma na imię Monika Brodka',
    'answers': ['Monika', 'Anna', 'Janusz', 'Hildegarda'],
    'correctAnswer': 'Janusz'
    },
    {'question': 'Która jest godzina?',
    'answers': ['W pół do komina', '8', '11', '16'],
    'correctAnswer': 'W pół do komina'        
    }
]
var usedIndexes = [];
var currentQuestionNum = selectQuestionNum();
var currentQuestion = questions[currentQuestionNum];


function selectQuestionNum(){
   var cqn = Math.floor(Math.random() * questions.length);
   if (usedIndexes.length < questions.length){
     while (usedIndexes.indexOf(cqn) != -1){
       cqn = Math.floor(Math.random() * questions.length);    
     }
    usedIndexes.push(currentQuestionNum);
 }
    else{
        cqn = -1;
    }
    return cqn;
}
function fillData(question)
{
   var questionBox = document.getElementById('question');
   var answersElems = document.getElementsByTagName('button');
   questionBox.innerHTML = question.question; 
    
   for(var i = 0; i < answersElems.length; i++)
       {
           answersElems[i].innerHTML = question.answers[i];
       }
}
fillData(currentQuestion);

function addListeners(){
    var btns = document.querySelectorAll('button');
    for(var i = 0; i < btns.length; i++)
        {
            btns[i].addEventListener('click', function(e){
                if(e.target.innerHTML == currentQuestion.correctAnswer)
                {
                    alert("Dobra odpowiedź!");
                    currentQuestionNum = Math.floor(Math.random() * questions.length);
                    currentQuestion = questions[currentQuestionNum];
                    fillData(currentQuestion);
                }
                                     
                else{
                alert("Zła odpowiedź. Koniec gry");
                }
            })
        }
}

addListeners();

// zamiast cqn = -1 można ustawić funkcję:
// function win(){
//document.getElementsByClassName('container')[0].innerHTML = //"Wygrana";}

